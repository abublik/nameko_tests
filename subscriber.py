from nameko.events import EventDispatcher, event_handler, SINGLETON, BROADCAST
from time import sleep
from datetime import datetime


class Subscriber:
    name = 'subscriber'

    @event_handler("publisher", "event_type", handler_type=BROADCAST,
            reliable_delivery=False)
    def handle_event(self, payload):
        now = datetime.now()
        sleep(payload)
        print(f"{now}: sub received from service_a message")

    @event_handler("publisher", "event_type", handler_type=BROADCAST,
            reliable_delivery=False)
    def handle_event2(self, payload):
        now = datetime.now()
        sleep(payload)
        print(f"{now}: sub2 received from service_a message")
