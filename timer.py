from nameko.timer import timer
from nameko.rpc import RpcProxy

from datetime import datetime


class Timer:
    name = 'timer'

    @timer(interval=2)
    def ping(self):
        datum = str(datetime.today())
        print(datum)
