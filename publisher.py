from nameko.events import EventDispatcher
from nameko.web.handlers import http
from nameko.rpc import rpc


class Publisher:
    name = 'publisher'

    dispatch = EventDispatcher()

    @http('GET', '/event/<int:payload>')
    def event(self, request, payload):
        print(payload)
        self.dispatching_method(payload)
        return 200, "ok"

    @rpc
    def dispatching_method(self, payload):
        print("dispatching")
        print(payload)
        self.dispatch("event_type", payload)
