import json

from nameko.rpc import rpc
from nameko.web.handlers import http


class GreetingService:
    name = 'greeting_service'

    @rpc
    def hello(self, name):
        print(name)
        return f"Hello, {name}!"

    @http('GET', '/hello/<string:name>')
    def get_hello(self, request, name):
        return self.hello(name)

    @http('GET', '/status/<int:number>')
    def status(self, request, number):
        return number, "haha"
