from nameko.rpc import rpc, RpcProxy

class Service2:
    name = 'service2'

    greeting_service = RpcProxy("greeting_service")

    @rpc
    def ping(self, msg):
        print("ping")
        return self.greeting_service.hello(msg)
