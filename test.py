from nameko.standalone.rpc import ClusterRpcProxy

config = {
    'AMQP_URI': "pyamqp://guest:guest@localhost"
}

with ClusterRpcProxy(config) as cluster_rpc:
    hello_res = cluster_rpc.service2.ping.call_async("hello")
    world_res = cluster_rpc.service2.ping.call_async("world")
    # do work while waiting
    print(hello_res.result())  # "hello-x-y"
    print(world_res.result())  # "world-x-y"
